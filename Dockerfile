FROM node:10-alpine
COPY . /app
RUN chown -R node /app
USER node
WORKDIR /app
RUN npm install --production

ENTRYPOINT ["npm"]
CMD ["start"]

EXPOSE 8080
