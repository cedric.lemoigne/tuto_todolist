const App = require('../lib/app')

async function main () {
  await new App().start()
}

main()
