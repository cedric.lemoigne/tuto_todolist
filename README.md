# TODO List

Back-end pour l'application de Todo List. Le client peut être trouvé à l'adresse
suivante: https://gitlab.com/cedric.lemoigne/tuto_todolist_client

Dans ce dépôt on pourra trouver:
- le Dockerfile pour lancer ce backend
- un fichier docker compose pour lancer l'application.

## Tester l'application en local

```bash
sudo echo "127.0.0.1 todos.loc" >> /etc/hosts
git clone git@gitlab.com:cedric.lemoigne/tuto_todolist_client.git
cd tuto_todolist_client
docker build -t tuto-frontend .
cd ..
git clone git@gitlab.com:cedric.lemoigne/tuto_todolist.git
cd tuto_todolist
docker build -t tuto-backend
docker-compose up -d

```
