const express = require('express')
const pkg = require('../package.json')
const TaskManager = require('./task-manager')
const DbClient = require('./db-client')
const { json } = require('body-parser')
const config = require('../config')

/**
 * Main class of the backend
 */
class App {
  constructor () {
    this.app = express()

    this.dbClient = new DbClient(config.url)
    this.app.use(json())
    /*
        1 tache { name: string , deadline: Date, done: boolean };
    */

    /*
      Route de test simple
    */
    this.app.get('/', function (req, res) {
      res.setHeader('ContentType', 'text/plain')
      res.send('Ok')
    })
    /*
      Route renvoyant la version
    */
    this.app.get('/version', function (req, res) {
      res.send(pkg.version)
    })

    /*
        Route de validation d'ajout d'une tâche
    */

    this.app.post('/add', (req, res) => {
      const task = req.body

      this.taskManager.add(task).then(result => {
        res.json(result)
      })
    })

    this.app.post('/edit', (req, res) => {
      const task = req.body
      task.deadline = new Date(task.deadline)
      this.taskManager.edit(task).then(result => {
        res.json(result)
      })
    })

    this.app.delete('/delete/:id', (req, res) => {
      this.taskManager.delete(req.params.id).then(result => {
        res.json(result)
      })
    })

    this.app.get('/tasks', (req, res) => {
      this.taskManager.getAll().then(tasks => {
        res.json({ 'success': true, 'tasks': tasks })
      })
        .catch(err => {
          res.json({ 'success': false, message: 'Impossible de récupérer les tâches ' + err })
        })
    })

    /* */
    this.app.use(function (req, res, next) {
      res.setHeader('Content-Type', 'text/plain')
      res.status(404).send('Page introuvable')
    })
  }
  /**
   * Sart the server and listen on the port 8080
   */
  async start () {
    this.dbClient.connect().then(() => {
      this.taskManager = new TaskManager(this.dbClient.db, config.todoCollection)
      this.server = this.app.listen(8080)
    })
  }
  /**
   * Stop the application
   */
  async stop () {
    this.dbClient.close()
    await this.server.close()
  }
}

module.exports = App
