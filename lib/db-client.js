const MongoClient = require('mongodb').MongoClient
const config = require('../config')
/**
 * Class used to manage the connection to the database
 */
class DbClient {
  constructor (url) {
    this.url = url
  }

  /**
   * Connect the client to the database
   * @returns {Promise}
   */
  async connect () {
    if (this.client) {
      this.close()
    }
    try {
      this.client = await MongoClient.connect(this.url, { useNewUrlParser: true })
      this.db = this.client.db(config.database)
    } catch (err) {
      console.log('An error occured while connecting to mongo', err)
    }
  }

  /**
   * Close the client's connection
   * @returns {Promise<void}
   */
  close () {
    if (this.client) {
      return this.client.close()
    }
    return Promise.reject(new Error('Pas de connection effective'))
  }
}

module.exports = DbClient
