const ObjectId = require('mongodb').ObjectID

/**
 * Class that manage all the action on the database for the Task entity.
 */
class TaskManager {
  constructor (db, collection) {
    this.db = db
    this.collection = this.db.collection(collection)
  }

  /**
   * Get all the records for the tasks
   * @returns {Promise}
   */
  getAll () {
    return this.collection.find({}).toArray()
  }

  /**
   * Add a new task in the database
   * @param {a task {name, deadline, done} } t the task to ass
   * @returns {a response object {success, message} }
   */
  async add (t) {
    let res = this.validateData(t)

    if (!res.success) {
      return res
    }

    await this.collection.insertOne(t).then((result) => {
      res = { success: true, value: t, message: 'tâche enregistrée!' }
    })
      .catch(err => {
        res = { success: false, message: err.message }
      })
    return res
  }
  /**
   * Delete a task in the mongodb database
   * @param {number} index the id of the task to delete
   * @returns {a response object {success, message} }
   */
  async delete (index) {
    let res
    await this.collection.deleteOne({ _id: ObjectId(index) }).then(result => {
      if (result.deletedCount > 0) {
        res = { success: true, message: 'tâche supprimée!' }
      } else {
        res = { success: false, message: 'impossible de supprimer la tâche! Elle n\'existe pas!' }
      }
    }).catch(_err => {
      res = { success: false, message: 'impossible de supprimer la tâche!' }
    })
    return res
  }

  /**
   * Edit the property of a task
   * @param {a task {_id, name, deadline, done} } t a task with a valid _id to be modified
   * @returns {a response object {success, message} }
   */
  async edit (t) {
    let res = this.validateData(t)
    if (!res.success) {
      return res
    }

    let tmp = {} // if update is done with t that contains an _id field, the update do not work
    tmp = Object.assign(tmp, t)
    delete tmp._id
    await this.collection.updateOne({ _id: ObjectId(t._id) }, { $set: tmp }).then(result => {
      if (result.modifiedCount === 0) {
        res = { success: false, message: 'impossible de modifier la tâche' }
      } else {
        res = { success: true, message: 'tâche modifiée!' }
      }
    }).catch(_err => {
      res = { success: false, message: 'impossible de modifier la tâche' }
    })

    return res
  }

  /**
   * Chek that a task has all the required fields.
   *
   * @param   {a task {_id, name, deadline, done} } t the task to verify
   * @returns {a response object {success, message} }
   */
  validateData (t) {
    if (t.name === 'undefined' || t.name === '') {
      return { sucess: false, message: 'Le nom de la tâche doit être fourni.' }
    }

    if (t.deadline === 'undefined') {
      return { success: false, message: 'La date d\'échéance doit être fournie.' }
    }

    t.deadline = new Date(t.deadline)

    if (!(t.deadline instanceof Date && !isNaN(t.deadline))) {
      return { success: false, message: 'La date doit être une date valide' }
    }

    return { success: true }
  }
}

module.exports = TaskManager
