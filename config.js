module.exports = {
  url: process.env.MONGO || 'mongodb://mongo:27017',
  database: 'todos_application',
  todoCollection: 'todos'
}
