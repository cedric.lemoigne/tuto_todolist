/* global describe,it */

const chai = require('chai')
chai.use(require('chai-datetime'))
const should = chai.should()
const request = require('supertest')
const App = require('../lib/app.js')
let falseTask = { name: 'faire la todo', deadline: new Date('2019/02/22'), done: false }
const config = require('../config')
const DbClient = require('../lib/db-client')
const dbClient = new DbClient(config.url)
const ObjectId = require('mongodb').ObjectID
const app = new App()

describe('Todolist api', () => {
  before(async () => {
    await dbClient.connect(config.url)
    return app.start()
  })

  beforeEach(async () => {
    dbClient.db.collection(config.todoCollection).deleteMany({})
  })

  describe('GET /tasks', () => {
    it('returns the default array at start', (done) => {
      request(app.app)
        .get('/tasks')
        .expect(res => {
          try {
            res.text.should.be.a('String')
            const _tasks = JSON.parse(res.text).tasks
            _tasks.should.be.an('array').with.lengthOf(0)
          } catch (err) {
            should.fail()
          }
        })
        .expect(200, done)
    })
  })

  describe('POST /add', () => {
    it('returns a success property (boolean), a value property (the task updated with its _id), and a message', (done) => {
      request(app.app)
        .post('/add')
        .send({ name: 'test1', deadline: new Date('2019/02/23'), done: false })
        .expect(res => {
          try {
            res.body.should.have.property('success', true)
            res.body.should.have.property('message', 'tâche enregistrée!')
            res.body.should.have.property('value')
            let value = res.body.value
            value.should.have.property('name', 'test1')
            value.should.have.property('_id')

            dbClient.db.collection(config.todoCollection).find({}).toArray()
              .then(todos => {
                todos.should.be.an('array').with.lengthOf(1)
                dbClient.db.collection(config.todoCollection).findOne(ObjectId(value._id)).then(element => {
                  value.should.have.property('name', element.name)
                })
              })
          } catch (err) {
            console.log(err.message)
            should.fail()
          }
        })
        .expect(200, done)
    })
  })

  describe('POST /edit', () => {
    it('valid edit action : returns a success property (boolean) and a message', (done) => {
      // first add an item to the empty database
      dbClient.db.collection(config.todoCollection).insertOne(falseTask).then(result => {
        request(app.app)
          .post('/edit')
          .send({ _id: falseTask._id, name: 'test2', deadline: new Date('2019/02/23'), done: false })
          .expect(res => {
            try {
              res.body.should.have.property('success', true)
              res.body.should.have.property('message', 'tâche modifiée!')
              dbClient.db.collection(config.todoCollection).findOne({ _id: falseTask._id }).then(result => {
                result.should.have.property('name', 'test2')
                result.should.have.property('deadline')
                result.deadline.should.equalDate(new Date('2019/02/23'))
              })
                .catch(err => {
                  console.log(err.message)
                  should.fail()
                })
            } catch (err) {
              console.log(err.message)
              should.fail()
            }
          })
          .expect(200, done)
      })
    })

    it('invalid edit action with unavailable Id: returns a success property (boolean) and a message', (done) => {
      dbClient.db.collection(config.todoCollection).insertOne(falseTask).then(result => {
        request(app.app)
          .post('/edit')
          .send({ _id: 30, name: 'test2', deadline: new Date('2019/02/23') })
          .expect(res => {
            try {
              res.body.should.have.property('success', false)
              res.body.should.have.property('message', 'impossible de modifier la tâche')
            } catch (err) {
              console.log(err.message)
              dbClient.db.collection(config.todoCollection).find({}).toArray()
                .then(todos => {
                  todos.should.be.an('array').with.lengthOf(1)
                })
              should.fail()
            }
          })
          .expect(200, done)
      })
    })
  })

  describe('DELETE /delete/:id ', () => {
    it('valid delete action: returns a success property (boolean), and a message', (done) => {
      dbClient.db.collection(config.todoCollection).insertOne(falseTask).then(result => {
        request(app.app)
          .delete('/delete/' + falseTask._id)
          .expect(res => {
            try {
              res.body.should.have.property('success', true)
              res.body.should.have.property('message', 'tâche supprimée!')
              dbClient.db.collection(config.todoCollection).find({}).toArray()
                .then(todos => {
                  todos.should.be.an('array').with.lengthOf(0)
                })
            } catch (err) {
              console.log(err.message)
              should.fail()
            }
          })
          .expect(200, done)
      })
    })

    it('invalid delete action: it returns a success property (boolean), and a message', (done) => {
      dbClient.db.collection(config.todoCollection).insertOne(falseTask).then(result => {
        request(app.app)
          .delete('/delete/000000000000')
          .expect(res => {
            try {
              res.body.should.have.property('success', false)
              res.body.should.have.property('message', 'impossible de supprimer la tâche! Elle n\'existe pas!')
              dbClient.db.collection(config.todoCollection).find({}).toArray()
                .then(todos => {
                  todos.should.be.an('array').with.lengthOf(1)
                })
            } catch (err) {
              console.log(err.message)
              should.fail()
            }
          })
          .expect(200, done)
      })
    })
  })

  after(async () => {
    await dbClient.close()
    return app.stop()
  })
})
